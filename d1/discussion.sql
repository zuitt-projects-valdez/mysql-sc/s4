-- add a 5 artists 
INSERT INTO artists (name) VALUES ('Taylor Swift');
INSERT INTO artists (name) VALUES ('Lady Gaga');
INSERT INTO artists (name) VALUES ('Justin Bieber');
INSERT INTO artists (name) VALUES ('Ariana Grande');
INSERT INTO artists (name) VALUES ('Bruno Mars');

-- Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Fearless', 
  '2008-1-1', 
  4
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Red', 
  '2012-1-1', 
  4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Fearless', 
  246, 
  'Pop rock', 
  4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Love Story', 
  213, 
  'Country pop', 
  4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'State of Grace', 
  253, 
  'Rock, alternative rock, arena rock', 
  5
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Red', 
  204, 
  'Country', 
  5
);

--Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'A Star is Born', 
  '2018-1-1', 
  5
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Born This Way', 
  '2011-1-1', 
  5
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Black Eyes', 
  151, 
  'Rock', 
  6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Shallow', 
  201, 
  'Country, rock, folk rock', 
  6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Born This Way', 
  252, 
  'Electropop', 
  7
);

-- Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Purpose', 
  '2015-1-1', 
  6
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Believe', 
  '2012-1-1', 
  6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Sorry', 
  132, 
  'Dancehall', 
  8
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Boyfriend', 
  251, 
  'Pop', 
  9
);

-- SECTION - Advanced Selects
SELECT * FROM songs WHERE id != 10;

-- Greater than or equal 
SELECT * FROM songs WHERE id > 10; 
SELECT * FROM songs WHERE id >= 10; 

-- Less than or equal 
SELECT * FROM songs WHERE id < 5; 
SELECT * FROM songs WHERE id <= 5; 

-- Get specific ids (IN) 
SELECT * FROM songs WHERE genre IN ('Pop', 'Rock');

-- Get specific ids (OR)
SELECT * FROM songs WHERE genre = 'Pop' OR genre = 'Rock';
SELECT * FROM songs WHERE genre = 'Pop' OR id = 2;

-- IN vs OR = in can only be used for one field whereas or can do multiple fields

-- Combining conditions 
SELECT * FROM songs WHERE album_id = 2 AND id < 5; 

-- Find partial matches 
SELECT * FROM songs WHERE song_name LIKE '%y';
-- creates a partial match with the last character % (partial match)
SELECT * FROM songs WHERE song_name LIKE '%y%'; --y in between
SELECT * FROM songs WHERE song_name LIKE 'y%'; --y in beginning

-- Sorting records; doesn't affect db just for output purposes 
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;
SELECT * FROM songs ORDER BY RAND(); -- random order; could be good for a featured products list and can feature 5 random products 

-- Limiting record output
SELECT * FROM songs LIMIT 3; -- gives you just the first 3 
SELECT * FROM songs ORDER BY RAND() LIMIT 3;

-- Getting distinct records 
SELECT DISTINCT genre FROM songs; -- shows each genre individually, no repeats 

-- [SECTION] Table Joins

-- Combine artists and albums tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table
SELECT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT songs.song_name, albums.album_title, artists.name FROM songs
	JOIN albums ON songs.album_id = albums.id
	JOIN artists ON albums.artist_id = artists.id;

SELECT songs.song_name, albums.album_title, artists.name FROM songs
	JOIN albums ON songs.album_id = albums.id
	JOIN artists ON albums.artist_id = artists.id WHERE songs.song_name = "Numb";

-- Using joins, display a single table ordered songs->albums->artists of all of Lady Gaga's songs. Show only the song name, album name, and artist name.

SELECT songs.song_name, albums.album_title, artists.name FROM songs
	JOIN albums ON songs.album_id = albums.id
	JOIN artists ON albums.artist_id = artists.id WHERE artists.name = "Lady Gaga";

SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;
